<?php

 defined('MOODLE_INTERNAL') || die();
 require_once(__DIR__ . '/../../../config.php'); // Do this.
// require_once(__DIR__ . '/../classes/local_request_course_form.php');

 class TestLocalRequestCourse extends \advanced_testcase {
     private $mform;

     public function test_form_validation_submit() {

         $formData = [
             'title' => 'Test Title',
             'content' => 'Content Test',
         ];
         local_request_course_form::mock_submit($formData);
         $form = new local_request_course_form(null);
         $submissiondata = $form->get_data();
         $this->assertEquals(3, 1+2);
     }
 }