<?php
/**
 * @package local_manage_request_course
 */
//namespace local_request_course\output;

// Make sure this isn't being directly accessed.
defined('MOODLE_INTERNAL') || die();

class local_request_course_renderer extends plugin_renderer_base {

    public function render_manage_course(array $sort, array $filter) {
        global $PAGE;

        $manage_request_course = new \local_request_course\output\manage_request_course($sort, $filter);
        // Include javascript to handle event on the page.
        return $this->render_from_template('local_request_course/manage_request_course', $manage_request_course->export_for_template($this));
    }
}