<?php

/**
 * Strings for component 'chat', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package   local_request_course
 */


$string['title'] = 'Title';
$string['content'] = 'Content';
$string['localrequestcourse'] = 'Local request course';
$string['managerequestcourse'] = 'Manage request course';
$string['editlocalcoursetitle'] = 'Edit title name';
$string['coursetitle'] = 'Link Title';
$string['creator'] = 'Creator';

$string['sortbytitle'] = 'Sort by title';
$string['sortbycontent'] = 'Sort by title';
$string['sortbycreator'] = 'Sort by title';

$string['filterbycreator'] = 'Filter by creator';
$string['filter'] = 'Filter';
$string['captchalrcourse'] = 'Recaptcha request course';