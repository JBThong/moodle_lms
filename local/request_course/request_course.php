<?php

require_once('../../config.php');
require_once('./classes/local_request_course_form.php');


global $DB, $USER;
require_login();
$title = get_string('localrequestcourse', 'local_request_course');
$userid = $USER->id;
$context = context_user::instance($userid);
$PAGE->set_url('/local/request_course/request_course.php');
$PAGE->set_context($context);
$PAGE->set_pagelayout('base');
$PAGE->set_title($title);
$PAGE->set_heading($title);

echo $OUTPUT->header();

$mform = new local_request_course_form();

//Form processing and displaying is done here
if ($mform->is_cancelled()) {
   //Handle form cancel operation, if cancel button is present on form
} else if ($fromform = $mform->get_data()) {
 //In this case you process validated data. $mform->get_data() returns data posted in form.

    $fromform->userid = $USER->id;
    $newrequestcourseid = $DB->insert_record('local_request_course', $fromform);
    echo "Add success!";
//    $urltogo= $CFG->wwwroot.'/local/request_course/manage_local_request_course_view.php';
//    redirect($urltogo);
} else {
 $mform->set_data($fromform);
 $mform->display();
}
echo $OUTPUT->footer();