<?php

namespace local_request_course;

defined('MOODLE_INTERNAL') || die();


class constants {
    const COURSE_UNIQUE_ID = 'local-request-course';
    const SORT_ASC = 'ASC';
    const SORT_DESC = 'DESC';
    const DEFAULT_PAGE_SIZE = 50;
}