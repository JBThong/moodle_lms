<?php

global $CFG, $DB;

require_once('../../config.php');
require_once("$CFG->libdir/formslib.php");

class local_request_course_form extends moodleform {
   //Add elements to form
    public function definition() {

       $mform = $this->_form;

       $mform->addElement('text', 'title', get_string('title', 'local_request_course')); // Add elements to your form
       $mform->addRule('title', 'Title is required', 'required', null, 'client');
       $mform->setType('title', PARAM_TEXT);


        $mform->addElement('textarea', 'content', get_string("content", "local_request_course"), 'wrap="virtual" rows="20" cols="50"');
       $mform->addRule('content', 'Content is required', 'required', null, 'client');

        $mform->addElement('recaptcha', 'captchalrcourse', get_string('captchalrcourse', 'local_request_course'));
       //normally you use add_action_buttons instead of this code
        $buttonarray=array();
        $buttonarray[] = $mform->createElement('submit', 'submitbutton', get_string('add'));
        $buttonarray[] = $mform->createElement('reset', 'resetbutton', get_string('reset'));
        $buttonarray[] = $mform->createElement('cancel');
        $mform->addGroup($buttonarray, 'buttonar', '', ' ', false);
        $mform->setType('buttonar', PARAM_TEXT);

    }
    //Custom validation should be added here
    function validation($data, $files) {
        global $CFG;

        $errors = parent::validation($data, $files);
        if (!empty($CFG->recaptchapublickey) && !empty($CFG->recaptchaprivatekey)) {
            $recaptcha_element = $this->_form->getElement('captchalrcourse');
            if (!empty($this->_form->_submitValues['recaptcha_challenge_field'])) {
                $challenge_field = $this->_form->_submitValues['recaptcha_challenge_field'];
                $response_field = $this->_form->_submitValues['recaptcha_response_field'];
                if (true !== ($result = $recaptcha_element->verify($challenge_field, $response_field))) {
                    $errors['captchalrcourse'] = $result;
                }
            } else {
                $errors['captchalrcourse'] = get_string('missingrecaptchachallengefield');
            }
        }

        return $errors;
    }
}