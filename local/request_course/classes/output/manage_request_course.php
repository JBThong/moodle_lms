<?php

namespace local_request_course\output;

use renderer_base;
use stdClass;
use renderable;
use templatable ;
use local_request_course\output;
use local_request_course\course_table;
use local_request_course\course_model;
use local_request_course\constants;

defined('MOODLE_INTERNAL') || die();

class manage_request_course implements renderable, templatable  {

    /**
     * @var $sort
     */
    protected $sort;
    protected $filter;

    /**
     * manage_request_course constructor.
     *
     * @param array $sort
     */
    public function __construct(array $sort, array $filter) {
        $this->sort = $sort;
        $this->filter = $filter;
    }

    /**
     * @param renderer_base $output
     * @return array|stdClass
     * @throws \coding_exception
     * @throws \moodle_exception
     */
    public function export_for_template(renderer_base $output) {
        global $OUTPUT, $USER;
        $courses = course_model::get_courses($this->filter);
        self::sort_data($courses, $this->sort['field'], $this->sort['type']);
        $data = new stdClass();
        $table = new course_table($courses, ['sort' => $this->sort]);
        $data->table = self::get_table_html($table, count($courses));
        return $data;
    }

    public static function get_table_html(course_table $table, $pagesize) {
        ob_start();
        $table->out($pagesize);
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }

    
    protected function sort_data(&$data, $field, $type = constants::SORT_ASC) {
        switch (strtoupper($type)) {
            case constants::SORT_ASC:
                $this->sort($data, $field, constants::SORT_ASC);
                break;
            case constants::SORT_DESC:
                $this->sort($data, $field, constants::SORT_DESC);
                break;
        }
    }


    private function sort(&$data, $field, $type) {
        usort($data, function ($a, $b) use ($field, $type) {
            return $type == constants::SORT_ASC ? strnatcasecmp($a->$field, $b->$field) : strnatcasecmp($b->$field, $a->$field);
        });
    }
}